#include "gkeycaps.h"
#include <gdk/gdkx.h>
#include <X11/Xos.h>
#include <X11/Xlib.h>

#define BUFLEN 10240

void add_space(GtkWidget * hbox1, int size)
{
   GtkWidget *da = gtk_drawing_area_new ();
   gtk_box_pack_start (GTK_BOX (hbox1), da, FALSE, FALSE, 0);
   gtk_widget_set_size_request (da, size, -1);
   gtk_widget_show (da);
}

void add_nbtn(GtkWidget * hbox1, gchar * text, int size)
{
   GtkWidget * button = gtk_toggle_button_new_with_mnemonic (text);
   gtk_widget_set_size_request (button, size, 24);
   gtk_box_pack_start (GTK_BOX (hbox1), button, FALSE, FALSE, 0);
   gtk_widget_show (button);
}

void add_pbtn(GtkWidget * hbox1, gchar * name, int size)
{
   GtkWidget * image;
   GtkWidget * button = gtk_toggle_button_new ();
   gtk_widget_set_size_request (button, size, 24);
   gtk_box_pack_start (GTK_BOX (hbox1), button, FALSE, FALSE, 0);
   gtk_widget_show (button);
   image = create_pixmap (gtk_widget_get_parent_window(hbox1), name);
   gtk_widget_show (image);
   gtk_container_add (GTK_CONTAINER (button), image);
}

void add_btn(GtkWidget * hbox1, gchar * name, int size)
{
  gchar fname[1024];
  struct stat statstruct;
  sprintf(fname,"%s/pixmaps/gkeycaps/%s.png",PACKAGE_DATA_DIR,name);
  if (!stat(&fname,&statstruct)) 
  {
   add_pbtn(hbox1,&fname,size);
  } else
  {
   /* semicolon -> ";" */
   add_nbtn(hbox1,name,size);
  }
}

gchar *** get_mappings ()
{
    int i;
    int kmin, kmax, kpk;
    KeySym * keymap;
    KeySym * origkeymap;
    gchar *** keytable;

    XDisplayKeycodes (GDK_DISPLAY(), &kmin, &kmax);
    origkeymap = XGetKeyboardMapping (GDK_DISPLAY(), kmin,
				      (kmax - kmin + 1),
				      &kpk);
				      
    keytable = malloc (sizeof(gchar *) * kmax);
				      
    if (!origkeymap) {
	fprintf (stderr, _("Gnome KeyCaps: unable to get keyboard mapping table.\n"));
	return;
    }

    keymap = origkeymap;

    for (i = kmin; i <= kmax; i++) {
	int  j, max;
	max = kpk - 1;
	keytable[i] = malloc(sizeof(gchar *) * kpk);
	while ((max >= 0) && (keymap[max] == NoSymbol))
	    max--;
	    for (j = 0; j <= max; j++) {
		    register KeySym ks = keymap[j];
		    char *s;
		    if (ks != NoSymbol)
			s = XKeysymToString (ks);
		    else
			s = "NoSymbol";
		    keytable[i][j]=s;
	}
	keymap += kpk;
    }
    XFree ((char *) origkeymap);
    return keytable;
}

void
parse_row (gchar *** kt, gchar * row, GtkWidget * vbox1)
{
  GtkWidget * hbox1;
  gchar * bp;
  gchar num = 0;
  gchar * open;
  int i;

  hbox1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox1);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox1, FALSE, FALSE, 0);
  gtk_widget_set_size_request (hbox1, -1, 26);

  bp = row;
  while (*bp != '\n') {
    if (!num) {
     switch (*(bp++)) {
       case('('):
       case('['): 
       case('<'): 
       case('{'): open=bp; num=1; break;
       case('-'): add_space(hbox1,24); break;
       case(' '): add_space(hbox1,5); break;
     }  /* switch */
    } else {
     switch (*(bp++)) {
       case(')'): 
        *(--bp)='\0';
        i = atoi(open);
        if ((i>512) || (i<0)) i=63;
        add_btn(hbox1,kt[i][0],24); 
        num=0;
        bp++; 
        break;
       case('>'): 
        *(--bp)='\0';
        i = atoi(open);
        if ((i>512) || (i<0)) i=63;
        add_btn(hbox1,kt[i][0],124); 
        num=0;
        bp++; 
        break;
       case(']'): 
        *(--bp)='\0';
        i = atoi(open);
        if ((i>512) || (i<0)) i=63;
        add_btn(hbox1,kt[i][0],35); 
        num=0;
        bp++; 
        break;
       case('}'): 
        *(--bp)='\0';
        i = atoi(open);
        if ((i>512) || (i<0)) i=63;
        add_btn(hbox1,kt[i][0],48); 
        num=0;
        bp++; 
        break;
     } /* switch */
    } /* if num */
  } /* while */
}

void
gkc_readconfig (GtkWidget * main, gchar * fn)
{
  FILE *f;
  GtkWidget *vbox;
  gchar buf[BUFLEN + 1];
  int i;
  gchar *** kt;

  f = fopen (fn, "r");
  kt = get_mappings();

  vbox = (GtkWidget *) g_object_get_data (G_OBJECT (main), "vbox1");

  for (i = 0; i < 7; i++)
    {
      if (fgets (buf, BUFLEN, f) != NULL)
	{
	  parse_row (kt,&buf, vbox);
	}
      else
	{
	  g_message ("SUCKS");
	}
    }
}
