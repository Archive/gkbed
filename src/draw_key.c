#include <gnome.h>
#include "gkbed.h"
#include "draw_key.h"
#include "interface.h"
#include "callbacks.h"
#include "editor.h"

static void
set_text (GnomeCanvasGroup * group, GnomeCanvasGroup * root,
	  double w, double h, guint type, guint code, double zoom)
{
	double a;
	GnomeCanvasItem * item;
	char buf[100];

/*	if (w < h) {
		a = w;
		w = h;
		h = a;
	}
*/	

	if (type == GKB_KEY_NUM) {
		item = gnome_canvas_item_new (root,
                               gnome_canvas_text_get_type (),
	                       "text", " ",
	                       "x", h * 0.2,
	                       "y", h * 0.17,
	                       "scale", zoom,
	                       "anchor", GTK_ANCHOR_NW, 
	                       "fill_color", "black",
	                       NULL);
	} else {
		item = gnome_canvas_item_new (root,
                               gnome_canvas_text_get_type (),
	                       "text", " ",
	                       "x", h * 0.2,
	                       "y", h * 0.17,
	                       "scale", zoom*1.2,
	                       "anchor", GTK_ANCHOR_NW, 
	                       "fill_color", "black",
	                       NULL);
	}
	if (item) {
	  	sprintf (buf, "text[%d][1]",code);
		g_object_set_data (G_OBJECT(group),buf,item);
		item = NULL;
	}
        item = gnome_canvas_item_new (root,
                               gnome_canvas_text_get_type (),
	                       "text", " ",
	                       "x", h * 0.2,
	                       "y", h - h * 0.17,
	                       "scale", zoom, 
	                       "anchor", GTK_ANCHOR_SW,
	                       "fill_color", "black",
	                       NULL);
	if (item) {
	  	sprintf (buf, "text[%d][0]",code);
		g_object_set_data (G_OBJECT(group),buf,item);
		item = NULL;
	}

        item = gnome_canvas_item_new (root,
                               gnome_canvas_text_get_type (),
	                       "text", " ",
	                       "x", w - h * 0.2,
	                       "y", h - h * 0.17,
	                       "scale", zoom,
	                       "anchor", GTK_ANCHOR_SE, 
	                       "fill_color", "black",
	                       NULL);
	if (item) {
	  	sprintf (buf, "text[%d][2]",code);
		g_object_set_data (G_OBJECT(group),buf,item);
		item = NULL;
	}
	                       
        item = gnome_canvas_item_new (root,
                               gnome_canvas_text_get_type (),
	                       "text", " ",
	                       "x", w / 2,
	                       "y", h / 2,
	                       "scale", zoom,
	                       "fill_color", "black",
	                       NULL);
	if (item) {
	  	sprintf (buf, "text[%d][3]",code);
		g_object_set_data (G_OBJECT(group),buf,item);
	}
}

static void
draw_key_shadow (GnomeCanvasGroup *group, double w, double h, guint32 fill)
{
	double d;
	GnomeCanvasPoints * points = gnome_canvas_points_new (6);

	if (w > h) d = h; else d = w;

        points->coords[0] = w - d * 0.15;
        points->coords[1] = d * 0.15;
        points->coords[2] = w - 1;
	points->coords[3] = 1;
	points->coords[4] = w - 1;
        points->coords[5] = h - 1;
	points->coords[6] = 1;
        points->coords[7] = h - 1;
	points->coords[8] = d * 0.15;
        points->coords[9] = h - d * 0.15;
	points->coords[10] = w - d * 0.15;
        points->coords[11] = h - d * 0.15;

       gnome_canvas_item_new (group,
		gnome_canvas_polygon_get_type (),
		"points", points,
		"fill_color_rgba", fill,
		NULL);
}

static void
draw_spec_key_shadow (GnomeCanvasGroup *group, double w, double h, double dx, guint32 fill)
{
	double d;
	GnomeCanvasPoints * points = gnome_canvas_points_new (6);

	if (w > h) d = h; else d = w;

        points->coords[0] = w - d * 0.15;
        points->coords[1] = d * 0.15;
        points->coords[2] = w - 1;
	points->coords[3] = 1;
	points->coords[4] = w - 1;
        points->coords[5] = h - 1;
	points->coords[6] = dx + 1;
        points->coords[7] = h - 1;
	points->coords[8] = dx + d * 0.15;
        points->coords[9] = h - d * 0.15;
	points->coords[10] = w - d * 0.15;
        points->coords[11] = h - d * 0.15;

       gnome_canvas_item_new (group,
		gnome_canvas_polygon_get_type (),
		"points", points,
		"fill_color_rgba", fill,
		NULL);
}

GnomeCanvasItem *
draw_spec_key_outer (GnomeCanvasGroup *group, double w, double h, double dx, double dy, guint32 color)
{
		GnomeCanvasPoints * points = gnome_canvas_points_new (6);
		GnomeCanvasItem * item;

	        points->coords[0] = 1;
	        points->coords[1] = 1;
	        points->coords[2] = w - 1;
		points->coords[3] = 1;
		points->coords[4] = w - 1;
	        points->coords[5] = h - 1;
		points->coords[6] = 1 + dx;
	        points->coords[7] = h - 1;
		points->coords[8] = 1 + dx;
	        points->coords[9] = dy - 1;
		points->coords[10] = 1;
	        points->coords[11] = dy - 1;
	
       		return gnome_canvas_item_new (group,
			gnome_canvas_polygon_get_type (),
			"points", points,
			"outline_color", "black",
			"fill_color_rgba", color,
			"width_units", 1.0,
			NULL);
}

static void
draw_spec_key_inner (GnomeCanvasGroup *group, double w, double h, double dx, double dy,guint32 fill, guint32 color)
{
	double d;
	GnomeCanvasPoints * points = gnome_canvas_points_new (6);

	if (w > h) d = h; else d = w;

        points->coords[0] = d * 0.15;
        points->coords[1] = d * 0.15;
	points->coords[2] = w - d * 0.15;
	points->coords[3] = d * 0.15;
	points->coords[4] = w - d * 0.15;
	points->coords[5] = h - d * 0.15;

	if (dx > 0) {
		points->coords[6] = 1 + dx + d * 0.15;
		points->coords[7] = h - d * 0.15;
		points->coords[8] = 1 + dx + d * 0.15;
		points->coords[9] = dy - 1 - d * 0.15;
		points->coords[10] = d * 0.15;
		points->coords[11] = dy - 1 - d * 0.15;
	} else {
		points->coords[6] = 1 + dx - d * 0.15;
		points->coords[7] = h - d * 0.15;
		points->coords[8] = 1 + dx - d * 0.15;
		points->coords[9] = dy - 1 + d * 0.15;
		points->coords[10] = d * 0.15;
		points->coords[11] = dy - 1 + d * 0.15;
	}
	
       	gnome_canvas_item_new (group,
			gnome_canvas_polygon_get_type (),
			"points", points,
			"outline_color_rgba", color,
			"fill_color_rgba", fill,
			NULL);
}

static void
draw_key_outer (GnomeCanvasGroup *group,double w,double h,guint32 fill)
{
	GnomeCanvasPoints * points = gnome_canvas_points_new (5);
	
	points->coords[0] = 1;
        points->coords[1] = 1;
        points->coords[2] = w - 1;
	points->coords[3] = 1;
        points->coords[4] = w - 1;
        points->coords[5] = h - 1;
	points->coords[6] = 1;
        points->coords[7] = h - 1;
        points->coords[8] = 1;
	points->coords[9] = 1;

        gnome_canvas_item_new (group,
                           gnome_canvas_polygon_get_type (),
                           "points", points,
                           "outline_color", "black",
                           "fill_color_rgba", fill,
                           "width_units", 1.0,
                           NULL);
}

static gint
key_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	static double x, y;
	GtkWidget * window;
	double new_x, new_y;
	GdkCursor *fleur;
	GtkWidget * popup;
	static int dragging;
	double item_x, item_y;
	
	window = (GtkWidget *) data;

	/* set item_[xy] to the event x,y position in the parent's item-relative coordinates */
	item_x = event->button.x;
	item_y = event->button.y;
	gnome_canvas_item_w2i (item->parent, &item_x, &item_y);
	
	switch (event->type) {
	case GDK_BUTTON_PRESS:
		switch (event->button.button) {
		case 3:
			if (event->button.state & GDK_SHIFT_MASK)
				gtk_object_destroy (GTK_OBJECT (item));
			else {
				x = item_x;
				y = item_y;

				fleur = gdk_cursor_new (GDK_FLEUR);
				gnome_canvas_item_raise_to_top (item);
				gnome_canvas_item_grab (item,
							GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
							fleur,
							event->button.time);
				gdk_cursor_unref (fleur);
				dragging = TRUE;
			}
			break;

		case 1:
			/* popup menu or edit */
			break;

		case 2:
			if (event->button.state & GDK_SHIFT_MASK)
				gnome_canvas_item_raise_to_top (item);
			else
				gnome_canvas_item_raise (item, 1);
			break;

		default:
			break;
		}

		break;

	case GDK_MOTION_NOTIFY:
		if (dragging && (event->motion.state & GDK_BUTTON3_MASK)) {
			new_x = item_x;
			new_y = item_y;

			gnome_canvas_item_move (item, new_x - x, new_y - y);
			x = new_x;
			y = new_y;
		}
		break;

	case GDK_BUTTON_RELEASE:
		switch(event->button.button) {
		case 3:
			gnome_canvas_item_ungrab (item, event->button.time);
			dragging = FALSE;
			break;
		case 1:
			editor_fill(item, window);
			break;
		}
		break;	
	default:
		break;
	}

	return FALSE;
}


static void
setup_key(GnomeCanvasItem *item, GtkWidget * window) {

	g_signal_connect (item, "event",
	                  G_CALLBACK (key_event), window);

}

GnomeCanvasGroup *
draw_key (GtkWidget * window, 
          GnomeCanvasGroup * root,
          GkbKey * key,
          double zoom)
{
	GnomeCanvasGroup  * group;
	GnomeCanvasPoints * points;
	GnomeCanvasItem * item;
	double affine[6];
	double w,h,d;
	guint32 sc;

	group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (root,
	                               gnome_canvas_group_get_type (),
	                               "x", key->x,
	                               "y", key->y,
	                               NULL));

	if (key->color == GKB_COLOR_NORMAL)
		sc = GKB_SHADOW_NORMAL;
	else 	sc = GKB_SHADOW_GRAY;

	if (key->face == GKB_NORMAL_KEY) {
		w = key->w;
		h = key->h;

		if (w <= 0.0) w = 50.0;
		if (h <= 0.0) h = 50.0;

		draw_key_outer  (group, w, h, key->color);
	 	draw_key_shadow  (group, w, h, sc);

		if (w > h) d = h; else d = w;

		points = gnome_canvas_points_new (5);
		points->coords[0] = d * 0.15;
		points->coords[1] = d * 0.15;
		points->coords[2] = w - d * 0.15;
		points->coords[3] = d * 0.15;
		points->coords[4] = w - d * 0.15;
		points->coords[5] = h - d * 0.15;
		points->coords[6] = d * 0.15;
		points->coords[7] = h - d * 0.15;
		points->coords[8] = d * 0.15;
		points->coords[9] = d * 0.15;

	        item = gnome_canvas_item_new (group,
			gnome_canvas_polygon_get_type (),
			"points", points,
			"outline_color_rgba", sc,
			"fill_color_rgba", key->color,
			"width_units", 1.0,
			NULL);

	} else {
		double dx;

		h = key->h + key->h2;
		w = key->w;

		dx = key->x2 - key->x;

		item = draw_spec_key_outer   (group, w, h, dx, key->h, key->color);
		draw_spec_key_shadow  (group, w, h, dx, sc);
		draw_spec_key_inner   (group, w, h, dx, key->h, key->color, sc);
	}

	switch (key->type) {
		case (GKB_KEY_NORMAL):
			set_text (root, group, w, h,
				key->type,key->code,zoom);
		break;
		case (GKB_KEY_NUM):
			set_text (root, group, w, h, 
				key->type,key->code,zoom);
		break;
		case (GKB_KEY_SPEC):
       			set_text (root, group, w, h,
				key->type, key->code,zoom);
		break;
	}

	art_affine_rotate (affine, key->angle);
	gnome_canvas_item_affine_relative (GNOME_CANVAS_ITEM(group), affine);
	setup_key(GNOME_CANVAS_ITEM(group),window);

	g_object_set_data (G_OBJECT(group), "keycode", (gpointer) key->code);

	return group;
}
