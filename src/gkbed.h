
#define GKB_COLOR_NORMAL   0xffffffff
#define GKB_COLOR_GRAY     0x999999ff
#define GKB_SHADOW_NORMAL  0x666666ff
#define GKB_SHADOW_GRAY    0x555555ff

#define GKB_KEY_NORMAL  0
#define GKB_KEY_NUM     1
#define GKB_KEY_SPEC    2

#define GKB_SPEC_KEY	0
#define GKB_NORMAL_KEY	1

typedef struct _GkbKey GkbKey;
typedef struct _GkbKeyLine GkbKeyLine;

struct _GkbKey
{
	double x,y,w,h,angle;
	double x2,y2,h2,w2;  	// polygon
	guint code;
	GList * chars;
	guint type;
	guint face;
	guint32 color;
};

struct _GkbKeyLine
{
	GkbKey ** keys;
	double x,y,w,h,angle;
};

GnomeCanvasGroup *
show_canvas(GtkWidget* window,gchar * filename, double x0, double y0, double zoom);
