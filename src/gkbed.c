#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "gkbed.h"
#include "xmodmap.h"
#include "draw_key.h"

GkbKey *
new_key (double angle, double x,double y,double w,double h,guint type,guint32 color)
{
	GkbKey * key = g_new0(GkbKey, sizeof(GkbKey));  

	key->x = x;
	key->y = y;
	key->w = w;
	key->h = h;
	key->angle = angle;
	key->type = type;
	key->color = color;
	key->face = GKB_NORMAL_KEY;

	return key;
}

GkbKey *
new_spec_key (double angle, double x,double y,double w,double h,
		double x2,double y2,double w2,double h2,guint type,guint32 color)
{
	GkbKey * key = g_new0(GkbKey, sizeof(GkbKey));  

	key->x = x;
	key->y = y;
	key->w = w;
	key->h = h;
	key->x2 = x2;
	key->y2 = y2;
	key->w2 = w2;
	key->h2 = h2;
	key->angle = angle;
	key->type = type;
	key->color = color;
	key->face = GKB_SPEC_KEY;

	return key;
}

static gint
keyb_event (GnomeCanvasGroup *group, GdkEvent *event, gpointer data)
{
	GdkEventKey *kevent;
	char buf[100];
	GnomeCanvasItem * item;
	GkbKey * key;

	switch (event->type) {
		case GDK_KEY_PRESS:
			kevent = (GdkEventKey *) event;
			sprintf(buf, "key_%d", kevent->hardware_keycode);
			item = g_object_get_data (G_OBJECT(group), buf);
			g_message("P: %d", kevent->hardware_keycode);
			if (item)
				gnome_canvas_item_set (GNOME_CANVAS_ITEM(item),
						"fill_color", "green",
						NULL);
			break;
		case GDK_KEY_RELEASE:
			kevent = (GdkEventKey *) event;
			sprintf(buf, "key_%d", kevent->hardware_keycode);
			item = g_object_get_data(G_OBJECT(group), buf);
			key = g_object_get_data (G_OBJECT(item), "key");
			g_message("R: %d", kevent->hardware_keycode);
			if (item) {
				gnome_canvas_item_raise_to_top (item);
				gnome_canvas_item_set (GNOME_CANVAS_ITEM(item),
						"fill_color_rgba", key->color,
						NULL);
			}
			break;
		default:
			break;
	}
	return FALSE;
}

GnomeCanvasGroup *
show_canvas(GtkWidget* window,gchar * filename, double x0, double y0, double zoom)
{
  GnomeCanvasGroup * group;
  GnomeCanvasGroup * item;
  GkbKey * key;
  GkbKey * key2;
  gchar * c;
  gchar ** cf;
  gchar * tcf;
  gchar ** cfg;
  gsize s;
  gint i;
  guint type, color;
  gint keycode;
  GtkWidget * canvas, * spinbutton;
  GError * e = NULL;
  gchar * tmp1, * tmp2;
  gchar *endptr;
  char buf[100];
  
  canvas = gtk_object_get_data (GTK_OBJECT (window), "canvas1");
  spinbutton = gtk_object_get_data (GTK_OBJECT (window), "spinbutton1");
  group = gnome_canvas_root(GNOME_CANVAS(canvas));

  g_file_get_contents (filename,&c,&s,&e);

  cf = g_strsplit (c,"\n",1024);

  g_message (_("Drawing characters..."));
  
  i = 0;
  if (*c != '\0')
  while (tcf = cf[i++]) {
  	// karakterenkent
  	if (strlen(tcf) <= 20) continue;
 	cfg = g_strsplit (tcf, "\t", 1024);

	if (cfg[2] != NULL) {

		if (!g_ascii_strcasecmp(cfg[3],"NUMBER")) type = GKB_KEY_NUM;
		else if (!g_ascii_strcasecmp(cfg[3],"SPEC")) type = GKB_KEY_SPEC;
		     else type = GKB_KEY_NORMAL;
		
		if (!g_ascii_strcasecmp(cfg[2],"GRAY")) color = GKB_COLOR_GRAY; 
		else color = GKB_COLOR_NORMAL;
		
		if (!g_ascii_strcasecmp(cfg[1],"RECT")) {
		  	key = new_key (g_strtod(cfg[4],NULL), 
		  		x0+g_strtod(cfg[5],NULL)*zoom,y0+g_strtod(cfg[6],NULL)*zoom, 
		      		g_strtod(cfg[7],NULL)*zoom,g_strtod(cfg[8],NULL)*zoom,
		      		type, color);
		} else {
		  	key = new_spec_key (g_strtod(cfg[4],NULL), 
		  		x0+g_strtod(cfg[5],NULL)*zoom,y0+g_strtod(cfg[6],NULL)*zoom, 
		      		g_strtod(cfg[7],NULL)*zoom,g_strtod(cfg[8],NULL)*zoom,
		  		x0+g_strtod(cfg[9],NULL)*zoom,y0+g_strtod(cfg[10],NULL)*zoom, 
		      		g_strtod(cfg[11],NULL)*zoom,g_strtod(cfg[12],NULL)*zoom,
	      			type, color);
		}

		endptr = NULL;
		key->code = (int) strtol(cfg[0],&endptr,10);

		item = draw_key (window,group,key,zoom);

		sprintf(buf,"key[%d]",key->code);
		g_object_set_data (G_OBJECT(group),buf,key);

	}
  }

/*    g_signal_connect (group, "event",
                   G_CALLBACK (keyb_event), NULL);
*/
    g_signal_connect (spinbutton, "value_changed",
                       G_CALLBACK (zoom_changed),                          
                       canvas);
                       
    return group; 
 }
