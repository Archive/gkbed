#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


void
on_properties1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_copy1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_paste1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_color1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gtk_main_quit();
}


void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save_as1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit2_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gtk_main_quit();
}


void
on_cut1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_copy2_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_paste2_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_clear1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_properties2_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  static GtkWidget *about;
  GtkWidget *link;
  gchar *file;
  GdkPixbuf *pixbuf;
  GError    *error = NULL;
                                                                                                                          
  static const gchar *authors[] = {
                "Szabolcs Ban <shooby@gnome.hu>",
                NULL
        };
  static const gchar *docauthors[] = {
                "Szabolcs Ban <shooby@gnome.hu>",
                NULL
        };
                                                                                                                          
  const gchar *translator_credits = _("translator_credits");
                                                                                                                          
  file = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_PIXMAP, "gkb-icon.png", FALSE, NULL);
  pixbuf = gdk_pixbuf_new_from_file (file, &error);
  g_free (file);
                                                                                                                          
  if (error) {
         g_warning (G_STRLOC ": cannot open %s: %s", file, error->message);
         g_error_free (error);
  }

  about = gnome_about_new (_("Gnome Keyboard Editor"),
                           VERSION,
                           _("(C) 1998-2000 Free Software Foundation"),
                           _("This application helps you to edit"
                             "keyboard maps. This will support xmodmap and "
			     "xkb layout files, and cfg files for metrics.\n"
                             "So long, and thanks for all the fish.\n"
                             "Thanks for Balazs Nagy (Kevin) "
                              "<julian7@iksz.hu> for his help "
                              "and Emese Kovacs <emese@gnome.hu> for "
                              "her solidarity."
                              "\nShooby Ban <shooby@gnome.hu>"),
                            authors,
                            docauthors,
                            strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
                            pixbuf);
                                                                                                                          
  link = gnome_href_new ("http://projects.gnome.hu/gkbed",
                         _("Gkbed Home Page (http://projects.gnome.hu/gkbed)"));
                                                                                                                          
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (about)->vbox), link, TRUE,
                      FALSE, 0);
                                                                                                                          
  if (pixbuf)
        g_object_unref (pixbuf);
                                                                                                                          
  gtk_window_set_wmclass (GTK_WINDOW (about), "keyboard layout switcher", "Gnome Keyboard Editor");
//  gtk_window_set_screen (GTK_WINDOW (about), gtk_widget_get_screen (gkbed));
  g_signal_connect (G_OBJECT(about), "destroy",
                          (GCallback)gtk_widget_destroyed, &about);
                                                                                                                          
  gtk_widget_show_all (about);
                                                                                                                          
  return;

}

void
zoom_changed (GtkWidget *range, gpointer data)
{
        gnome_canvas_set_pixels_per_unit (GNOME_CANVAS(data), GTK_SPIN_BUTTON(range)->adjustment->value / 100);
}

