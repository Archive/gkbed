#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <stdio.h>

#include "interface.h"
#include "support.h"
#include "gkbed.h"
#include "editor.h"

int
main (int argc, char *argv[])
{
  GtkWidget *window1;
  GnomeCanvasGroup * root;
  gchar * filename;
  gchar * modmap;

  if (argc >= 2) {
   if (argc >= 3) { 
    modmap = argv[2];
   }
   filename = argv[1];
  } else {
   filename = PACKAGE_DATA_DIR"/gnome/gkbed/kb-105.cfg";
   modmap   = PACKAGE_DATA_DIR"/xmodmap/xmodmap.hu";
  }

  g_message("%s, %s, %d",filename,argv[0],argc);

#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE,
                      argc, argv,
                      GNOME_PARAM_APP_DATADIR, PACKAGE_DATA_DIR,
                      NULL);

  window1 = create_editorwindow ();
  root = show_canvas(window1,filename,3.0,60.0,0.85);
  gtk_widget_show (window1);
  xmodmap_load (root,modmap);
  xmodmap_save (root,"gkbed.out");
  setup_editor (window1);
  
  gtk_main ();
  return 0;
}

