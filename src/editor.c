#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "gkbed.h"
#include "xmodmap.h"
#include "draw_key.h"
#include "editor.h"
#include "set_key.h"

static gboolean pml = 1;

static void
editor_set_text(GtkWidget * combo, gchar * str)
{
 if (!str) str = "";
 g_message("settext %s",str);
 gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(combo)->entry), g_strdup(str));
}

void
editor_fill(GnomeCanvasItem * item, GtkWidget * window) {
 GtkWidget * canvas = gtk_object_get_data (GTK_OBJECT(window), "canvas1");
 GnomeCanvasGroup * root = gnome_canvas_root (GNOME_CANVAS(canvas));
 GtkWidget * keycodeentry = gtk_object_get_data (GTK_OBJECT (window), "keycodeentry");
 GtkWidget * key1combo = gtk_object_get_data (GTK_OBJECT (window), "key1combo");
 GtkWidget * key2combo = gtk_object_get_data (GTK_OBJECT (window), "key2combo");
 GtkWidget * key3combo = gtk_object_get_data (GTK_OBJECT (window), "key3combo");
 GtkWidget * key4combo = gtk_object_get_data (GTK_OBJECT (window), "key4combo");
 GtkWidget * key5combo = gtk_object_get_data (GTK_OBJECT (window), "key5combo");
 gint keycode = gtk_object_get_data(G_OBJECT(item), "keycode");
 char buf[10];

 g_message ("[%d]",keycode);

 pml = 0;

 editor_set_text(key1combo,"");
 editor_set_text(key2combo,"");
 editor_set_text(key3combo,"");
 editor_set_text(key4combo,"");
 editor_set_text(key5combo,"");

 sprintf(buf,"%d",keycode); 
 gtk_entry_set_text(GTK_ENTRY(keycodeentry),g_strdup((gchar *)&buf));

 sprintf(buf,"xcode[%d][0]",keycode);
 editor_set_text(key1combo, g_object_get_data (G_OBJECT(root),buf));

 sprintf(buf,"xcode[%d][1]",keycode);
 editor_set_text(key2combo, g_object_get_data (G_OBJECT(root),buf));

 sprintf(buf,"xcode[%d][2]",keycode);
 editor_set_text(key3combo, g_object_get_data (G_OBJECT(root),buf));

 sprintf(buf,"xcode[%d][3]",keycode);
 editor_set_text(key4combo, g_object_get_data (G_OBJECT(root),buf));

 pml = 1;
}

static void editor_changed (GtkWidget * widget, GtkWidget * window) {
 GtkWidget * keycodeentry = gtk_object_get_data (GTK_OBJECT (window), "keycodeentry");
 GtkWidget * key1combo = gtk_object_get_data (GTK_OBJECT (window), "key1combo");
 GtkWidget * key2combo = gtk_object_get_data (GTK_OBJECT (window), "key2combo");
 GtkWidget * key3combo = gtk_object_get_data (GTK_OBJECT (window), "key3combo");
 GtkWidget * key4combo = gtk_object_get_data (GTK_OBJECT (window), "key4combo");
 GtkWidget * key5combo = gtk_object_get_data (GTK_OBJECT (window), "key5combo");
 GtkWidget * canvas = g_object_get_data (G_OBJECT(window),"canvas1");
 GnomeCanvasGroup * keys = gnome_canvas_root (GNOME_CANVAS(canvas));
 gint keycode = atoi(gtk_entry_get_text (keycodeentry));
 char buf[100];
 gchar * t1, * t2, * t3, * t4;

 if (keycode && pml) {
  t1 = gtk_entry_get_text (GTK_ENTRY(GTK_COMBO(key1combo)->entry));
  t2 = gtk_entry_get_text (GTK_ENTRY(GTK_COMBO(key2combo)->entry));
  t3 = gtk_entry_get_text (GTK_ENTRY(GTK_COMBO(key3combo)->entry));
  t4 = gtk_entry_get_text (GTK_ENTRY(GTK_COMBO(key4combo)->entry));

  sprintf (buf,"= %s %s %s %s",t1,t2,t3,t4);
  g_message("%s, %d [%d]",&buf,strlen((gchar *)&buf),keycode);
  set_keyface (keys,keycode,(gchar *)&buf,strlen((gchar *)&buf));
 }
}

void
setup_editor(GtkWidget * window) {
 GtkWidget * keycodeentry = gtk_object_get_data (GTK_OBJECT (window), "keycodeentry");
 GtkWidget * key1combo = gtk_object_get_data (GTK_OBJECT (window), "key1combo");
 GtkWidget * key2combo = gtk_object_get_data (GTK_OBJECT (window), "key2combo");
 GtkWidget * key3combo = gtk_object_get_data (GTK_OBJECT (window), "key3combo");
 GtkWidget * key4combo = gtk_object_get_data (GTK_OBJECT (window), "key4combo");
 GtkWidget * key5combo = gtk_object_get_data (GTK_OBJECT (window), "key5combo");

 g_signal_connect (GTK_COMBO(key1combo)->entry, "changed",
                   G_CALLBACK (editor_changed), 
                   window);
 g_signal_connect (GTK_COMBO(key2combo)->entry, "changed",
                   G_CALLBACK (editor_changed), 
                   window);
 g_signal_connect (GTK_COMBO(key3combo)->entry, "changed",
                   G_CALLBACK (editor_changed), 
                   window);
 g_signal_connect (GTK_COMBO(key4combo)->entry, "changed",
                   G_CALLBACK (editor_changed), 
                   window);
}
