#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <gnome.h>
#include <ctype.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "gkbed.h"
#include "draw_key.h"

static  gchar * ctable[100] =
         {"Control_L",  "Ctrl",
          "Control_R",  "Ctrl",
          "Shift_L",    "Shift",
          "Shift_R",    "Shift",
          "Return",     "Enter",
          "Mode_switch","AltGr",
          "Caps_Lock",  "Caps",
          "Select",     "Win",
          "Delete",     "Del",
          "Alt_L",      "Alt",
          "Alt_R",      "Alt",
          "Tab",        "Tab",
          "Multi_key",  "Menu",
          "Escape",	"Esc",
          "Num_Lock",	"Num",
          "KP_0",	"0",
          "KP_1",	"1",
          "KP_2",	"2",
          "KP_3",	"3",
          "KP_4",	"4",
          "KP_5",	"5",
          "KP_6",	"6",
          "KP_7",	"7",
          "KP_8",	"8",
          "KP_9",	"9",
          "KP_Home",	"Home",
          "KP_Multiply","*",
          "KP_Add",	"+",
          "KP_Divide",	"/",
          "KP_Subtract","-",
          "KP_Decimal", ".",
          "KP_Enter",	"Enter",
          "Scroll_Lock","Scroll",
          "Find",	"Pause",
          NULL, NULL};

gchar *
conv_name (gchar * name){
 int i;
 gchar * refname;

 i = 0;
 while (refname = ctable[i++]) {
//  g_message("%d, %s, %s",i, name,refname);
  if (strlen(name) == strlen(refname))
   if (!strncmp(name, refname, (int) strlen(name))) {
    return ctable[i];
  } /* if cmp */
  i++;
 } /* while refname */
 return name;
}

gchar *
rev_conv_name (gchar * name){
 int i;
 gchar * refname;

 i = 0;
 while (refname = ctable[++i]) {
  g_message("%d, %s, %s",i, name,refname);
  if (strlen(name) == strlen(refname))
   if (!strncmp(name, refname, (int) strlen(name))) {
    return ctable[i-1];
  } /* if cmp */
  i++;
 } /* while refname */
 return name;
}
