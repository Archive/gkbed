int 
skip_word (char *s, int len)
{
    register int n;

    n = skip_chars (s, len);
    return (n + skip_space (s+n, len-n));
}

int 
skip_chars(char *s, int len)
{
    register int i;

    if (len <= 0 || !s || *s == '\0') return (0);

    for (i = 0; i < len; i++) {
	if (isspace(s[i])) break;
    }
    return (i);
}

int 
skip_space(char *s, int len)
{
    register int i;

    if (len <= 0 || !s || *s == '\0') return (0);

    for (i = 0; i < len; i++) {
	if (!s[i] || !isspace(s[i])) break;
    }
    return (i);
}

int 
skip_until_char(char *s, int len, char c)
{
    register int i;

    for (i = 0; i < len; i++) {
	if (s[i] == c) break;
    }
    return (i);
}
