#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <gnome.h>
#include <ctype.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "gkbed.h"
#include "draw_key.h"
#include "util.h"
#include "set_key.h"
#include "keyconv.h"

static void do_empty  (char *line, int len, GnomeCanvasGroup * group);
static void do_keycode(char *line, int len, GnomeCanvasGroup * group);

static struct dt {
    char *command;			/* name of input command */
    int length;				/* length of command */
    void (*proc)(char *, int, GnomeCanvasGroup *); /* handler */
} dispatch_table[] = {
    { "keycode", 7, do_keycode },
    { "keysym",  6, do_empty   },
    { "add",     3, do_empty   },
    { "remove",  6, do_empty   },
    { "clear",   5, do_empty   },
    { "pointer", 7, do_empty   },
    { NULL,      0, NULL       }};

/*
 * do_keycode - parse off lines of the form
 *
 *                 "keycode" number "=" [keysym ...]
 *                           ^
 *
 * where number is in decimal, hex, or octal.  Any number of keysyms may be
 * listed.
 */

static void 
do_empty(char *line, int len,GnomeCanvasGroup * group)
{
}

static void 
do_keycode(char *line, int len,GnomeCanvasGroup * group)
{
    int dummy;
    char *fmt = "%d";
    int keycode;

    if (len < 3 || !line || *line == '\0') {  /* 5=a minimum */
	g_message ("Bad keycode input line: %s",line);
	return;
    }

    /*
     * We need not bother to advance line/len past the
     * number (or the string 'any') as finish_keycodes() will
     * first advance past the '='.
     */
    if (!strncmp("any", line, 3)) {
	keycode = 0;
    } else {
	if (*line == '0') line++, len--, fmt = "%o";
	if (*line == 'x' || *line == 'X') line++, len--, fmt = "%x";

	dummy = 0;
	if (sscanf (line, fmt, &dummy) != 1 || dummy == 0) {
	    g_message ("Bad keycode value");
	    return;
	}
	keycode = dummy;

	if ((int)keycode < 1 || (int)keycode > 200) {
	    g_message ("Bad keycode value (out of range)");
	    return;
	}
    }

    set_keyface (group, keycode, line, len);
}

/*
 * do_keysym - parse off lines of the form
 *
 *                 "keysym" keysym "=" [keysym ...]
 *                          ^
 *
 * The left keysyms has to be checked for validity and evaluated.
 */

void 
handle_line(char *line, int len, GnomeCanvasGroup * group)
{
    int n;
    struct dt *dtp;

    n = skip_chars (line, len);
    if (n < 0) return;

    for (dtp = dispatch_table; dtp->command != NULL; dtp++) {
	if (n == dtp->length &&
	    strncmp (line, dtp->command, dtp->length) == 0) {

	    n += skip_space (line+n, len-n);
	    line += n, len -= n;

	    (*(dtp->proc)) (line, len, group);
	    return;
	}
    }
}

void
xmodmap_load (GnomeCanvasGroup * group,gchar * filename)
{
  GnomeCanvasItem  * item;
  GList *  list;
  gchar *  file;
  gchar ** lines;
  gchar *  line;
  gchar ** cfg;
  gchar ** pair;
  gchar ** tmp;
  gsize    s;
  gint     i;
  guint    type, color;
  gint     keycode;
  GtkWidget * canvas;
  GError    * e = NULL;
  gchar     * tmp1, * tmp2;
  gchar     * endptr;
  char     buf[100];

  g_file_get_contents (filename,&file,&s,&e);
  lines = g_strsplit (file,"\n",1024);  

  i = 0;
  while (line = lines[i++]) {
   handle_line(line,strlen (line),group);
  }
}

void
xmodmap_save (GnomeCanvasGroup * group,gchar * filename)
{
  int i;
  gchar * text;
  char buf[100];
  gint fd;

  fd = open (filename,O_WRONLY | O_APPEND | O_CREAT, 0644);
  for (i=0;i<255;i++) {
	sprintf(buf,"xcode[%d][0]",i);
	if(text = g_object_get_data (G_OBJECT(group),buf)) {
	   sprintf (buf,"keycode %d = %s",i,text);
	   write (fd, &buf, strlen ((char *)&buf));
//	   g_message(buf);
	 
   	sprintf(buf,"xcode[%d][1]",i);
   	if(text = g_object_get_data (G_OBJECT(group),buf)) {
   	 sprintf (buf," %s",text);
   	 write (fd, &buf, strlen ((char *)&buf));
   	}

   	sprintf(buf,"xcode[%d][2]",i);
   	if(text = g_object_get_data (G_OBJECT(group),buf)) {
   	 sprintf (buf," %s",text);
   	 write (fd, &buf, strlen ((char *)&buf));
   	}

   	sprintf(buf,"xcode[%d][3]",i);
   	if(text = g_object_get_data (G_OBJECT(group),buf)) {
   	 sprintf (buf," %s",text);
   	 write (fd, &buf, strlen ((char *)&buf));
   	}
		write(fd,"\n",1);
	 }
  }
  close (fd);
}
