#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <gnome.h>
#include <ctype.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "gkbed.h"
#include "draw_key.h"
#include "util.h"

void 
set_keyface (GnomeCanvasGroup * group, int keycode, char *line, int len)
{
    GnomeCanvasItem * items[4];
    int n,i,j;
    union op *uop;
    struct op_keycode *opk;
    gchar * name;
    char buf[100];
    KeySym keysym, ukeysym, fkeysym;
    GkbKey * key;
    gchar * kname, * conv;
    gchar ** list;
   
    n = skip_until_char (line, len, '!');
    len = n + 1;
    line[n] = '\0';

    n = skip_until_char (line, len, '=');
    line += n, len -= n;
    
    if (len < 1 || *line != '=') {	/* = minimum */
	g_message ("keycode command (missing keysym list),");
	return;
    }
    line++, len--;			/* skip past the = */

    n = skip_space (line, len);
    line += n, len -= n;

//    g_message ("line: %s",line);

    sprintf(buf,"key[%d]",keycode);
    if (key = g_object_get_data (G_OBJECT(group),buf)) {

      sprintf(buf,"text[%d][0]",keycode);
      items[0] = g_object_get_data(G_OBJECT(group),buf);
      sprintf(buf,"text[%d][1]",keycode);
      items[1] = g_object_get_data(G_OBJECT(group),buf);
      sprintf(buf,"text[%d][2]",keycode);
      items[2] = g_object_get_data(G_OBJECT(group),buf);
      sprintf(buf,"text[%d][3]",keycode);
      items[3] = g_object_get_data(G_OBJECT(group),buf);

      list = g_strsplit (line, " ", 1024);

      i = j = 0;

      setlocale(LC_CTYPE,"en_US.UTF8");

      while (name = list[i++]) {
  	     if (name[0] == '\0') continue;
  	     keysym = XStringToKeysym (name);
  	     ukeysym = gdk_keyval_to_upper (keysym);
  	     if (!j) fkeysym = ukeysym;

//  	     g_message ("%s [%lc]> %s",name, keysym, line);

	     sprintf(buf,"xcode[%d][%d]",keycode,j);
	     g_object_set_data (G_OBJECT(group),buf,name);
  	     
  	      switch (key->type) {
  	       case GKB_KEY_SPEC:
  	        if (j+3 > 3) continue;
  	        if (items[j+3]) 
  	        	gnome_canvas_item_set(items[j+3],
  	        				"text",conv_name(name),
  	        				NULL);
//	        g_message("     spec: %s",name);
  	        break;
  	       case GKB_KEY_NUM:
  		sprintf(buf,"%lc",keysym);
  	        if (items[j])
  	        	gnome_canvas_item_set(items[j],
  	        				"text",buf,
  	        				NULL);
//  	        	g_message("      num: %s",buf);
  	        break;
  	       default:
  	        if (j == 1 || fkeysym == keysym) {
  	          ++j;
  	          continue;
  	        }
//  	        setlocale(LC_CTYPE,"en_US.UTF8");
                sprintf(buf,"%lc",ukeysym);
  	        if (items[j+(j<2)])
  	                gnome_canvas_item_set(items[j+(j<2)],
  	                			"text",&buf,
  	                			NULL);
//  		        g_message("     norm: %s, %d",buf,strlen(buf));
  	       } /* switch type */
  	    j++;
  	} /* while lastc */
     } else { 
      g_message ("no key for %d (%lc)",keycode, keycode); 
     }
}
